import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-greet',
  templateUrl: './greet.page.html',
  styleUrls: ['./greet.page.scss'],
})
export class GreetPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    const user = localStorage.getItem('User')
    if (user==null){
      this.router.navigateByUrl('/login',{replaceUrl:true})
    }
  }
  logout(){
    localStorage.removeItem('User')
    this.router.navigateByUrl('/login',{replaceUrl:true})
  }

}
