import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  name:string
  email:string
  password:string
  contactno:number
  country:string
  isLoading: boolean=false
  constructor(private http: HttpClient,private router: Router,private alertController: AlertController) { }

  ngOnInit() {
  }
  register(){
    this.isLoading= true
    let user={
      name:this.name,
      email:this.email,
      password:this.password,
      contactno:this.contactno,
      country:this.country
    }
    this.http.post('http://localhost:3000/users/register',user).subscribe(res=>{
      this.isLoading= false
      console.log(user)
      this.router.navigateByUrl('/login',{replaceUrl:true})

    },error=>{
      this.isLoading= false
      this.presentAlert('Registration Failed',error.error.error)
    })
  }
  login(){

    this.router.navigateByUrl('/login',{replaceUrl:true})
  }
  async presentAlert(header:string,message:string) {
    const alert = await this.alertController.create({
      header: header,
      message: message,
      buttons: ['OK'],
    });
    await alert.present();
  }

}
